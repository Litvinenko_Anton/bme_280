/***************************************************************************
  This is a library for the BME280 humidity, temperature & pressure sensor

  Designed specifically to work with the Adafruit BME280 Breakout
  ----> http://www.adafruit.com/products/2650

  These sensors use I2C or SPI to communicate, 2 or 4 pins are required
  to interface. The device's I2C address is either 0x76 or 0x77.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit andopen-source hardware by purchasing products
  from Adafruit!

  Written by Limor Fried & Kevin Townsend for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ***************************************************************************/

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_BME280.h>
#include <Adafruit_CCS811.h>
#include <OLED_I2C.h>

#define BME_SCK 13
#define BME_MISO 12
#define BME_MOSI 11
#define BME_CS 10
#define SEALEVELPRESSURE_HPA (1013.25)
#define DISP_CLK            10     // выход на семисегментный LED дисплей
#define DISP_DIO            11     // выход на семисегментный LED дисплей


OLED  myOLED(SDA, SCL, 8); // инициализация OLED дисплея
extern uint8_t SmallFont[];

Adafruit_BME280 bme; // BME280 - I2C
Adafruit_CCS811 ccs; // CCS811 - I2C
//Adafruit_BME280 bme(BME_CS); // hardware SPI
//Adafruit_BME280 bme(BME_CS, BME_MOSI, BME_MISO, BME_SCK); // software SPI

unsigned long delayTime = 500;

void setup() {
  initSerial();
  initOLED();
  initCCS811();
}

void initSerial() {
  Serial.begin(9600);
  Serial.println(F("BME280 test"));

  if (!bme.begin()) { // check status
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    //  while (1);
  }

  Serial.println("-- Default Test --");
  Serial.println();
}

void initOLED() {
  myOLED.begin();            // инициализация OLED дисплея
  myOLED.setFont(SmallFont); // инициализация шрифта
}

void initCCS811() {
  if (!ccs.begin()) {        // инициализация CCS811 CO2
    Serial.println("Failed to start sensor CCS811! Please check your wiring.");
    while (1);
  }

  //calibrate temperature sensor
  while (!ccs.available());
  float temp = ccs.calculateTemperature();
  ccs.setTempOffset(temp - 25.0);
}

  float temp = 0;     // температура с датчика (*C)
  float humidity = 0; // влажность с датчика  (%)
  float pressure = 0; // давление с датчика  (hPa)
  float altitude = 0; // высота над уровнем моря (m)

  float tempCCS = 0;
  float TVOC = 0;
  float eCO2 = 0;

void loop() {
  temp = bme.readTemperature(); 
  humidity = bme.readHumidity();
  pressure = bme.readPressure() / 100.0F;
  altitude = bme.readAltitude(SEALEVELPRESSURE_HPA); 
  
  if (ccs.available()) {
    if (!ccs.readData()) {
      tempCCS = ccs.calculateTemperature();
      TVOC = ccs.getTVOC();
      eCO2 = ccs.geteCO2();
    } else {
      Serial.println("ERROR!");
      while (1);
    }
  }

  printOLED(temp, humidity, pressure, tempCCS, TVOC, eCO2);
  printValuesSerial(temp, humidity, pressure, altitude, tempCCS, TVOC, eCO2);

  delay(delayTime);
}

int topT = 5;
int topH = 15;
int topP = 25;
int topTT = 35;
int topPPB = 45;
int topCO2 = 55;
int startNum = 32;
int startChar = 64;
int startCharFloat = 76;

void printOLED(float temp, float humidity, float pressure, float tempCCS, float TVOC, float eCO2) {

  myOLED.print("Temp: ", 0, topT);
  myOLED.printNumF(temp, 2, startNum, topT);
  myOLED.print("C", startChar, topT);

  myOLED.print("Hum: ", 0, topH);
  myOLED.printNumF(humidity, 2, startNum, topH);
  myOLED.print("%", startChar, topH);

  myOLED.print("Pres: ", 0, topP);
  myOLED.printNumF(pressure, 2, startNum, topP);
  myOLED.print("hPa", startCharFloat, topP);

  //----------//

  myOLED.print("Temp: ", 0, topTT);
  myOLED.printNumF(tempCCS, 2, startNum, topTT);
  myOLED.print("C", startChar, topTT);

  myOLED.print("TVOC: ", 0, topPPB);
  myOLED.printNumF(TVOC, 2, startNum, topPPB);
  myOLED.print("ppb", startCharFloat, topPPB);

  myOLED.print("eCO2: ", 0, topCO2);
  myOLED.printNumF(eCO2, 2, startNum, topCO2);
  myOLED.print("ppm", startCharFloat, topCO2);

  myOLED.update();

  //  myOLED.fillScr();
  //  myOLED.setPixel(10, 10);
  //  myOLED.print("A", 50, 20);
  //  myOLED.print("Temp: ", 50, 20);
  //  myOLED.printNumI(1234, 50, 20);
  //  myOLED.printNumF(12.55, 2, 50, 20);
  //  myOLED.drawLine(20, 10, 40, 10);
  //  myOLED.drawRect(20, 10, 40, 50);
  //  myOLED.drawRoundRect(20, 10, 40, 50);
  //  myOLED.drawCircle(30, 30, 20);
}

void printValuesSerial(float temp, float humidity, float pressure, float altitude, float tempCCS, float TVOC, float eCO2) {

  Serial.print("Temperature = ");
  Serial.print(temp);
  Serial.println(" *C");

  Serial.print("Humidity = ");
  Serial.print(humidity);
  Serial.println(" %");

  Serial.print("Pressure = ");
  Serial.print(pressure);
  Serial.println(" hPa");

  Serial.print("Approx. Altitude = ");
  Serial.print(altitude);
  Serial.println(" m");

  //----------//

  Serial.print("Temp = ");
  Serial.print(tempCCS);
  Serial.println(" *C");

  Serial.print("TVOC = ");
  Serial.print(TVOC);
  Serial.println(" ppb");

  Serial.print("eCO2 = ");
  Serial.print(eCO2);
  Serial.println(" ");

  Serial.println();
}
